# The Intelligent House

## Introduction

This is a school project for an embedded system to function as a "smart house", programmed in C.

The board being used is the Arduino Mega board (ATMega2560).

You can read a lot more over on the wiki pages:
- [Wiki Home Page](https://gitlab.com/InuitViking/the-intelligent-house/-/wikis/Home)
- [Libraries](https://gitlab.com/InuitViking/the-intelligent-house/-/wikis/Libraries)
- [HIPO Diagram](https://gitlab.com/InuitViking/the-intelligent-house/-/wikis/HIPO)
- [Wiring and Drawings](https://gitlab.com/InuitViking/the-intelligent-house/-/wikis/Wiring-and-Drawings)

## Project description
Here are the things that I wanted to achieve:
- [x] Numbered keypad for typing a PIN.
   - [ ] If there was time, some minor hashing of the PIN.
   - [ ] If there was time, storing in EEPROM (really should have waited with trying this out).
- [x] An LCD display to show the typing of the PIN, but replaced by an other character (dots or asterisk)
   - This was "removed", because I couldn't get it to work with interrupts.
   - The code is there, but is commented out.
- [x] A stepper motor to "unlock a door".
  - The stepper motor vibrates, rather than turns.
- [x] Temperature management:
   - [x] Thermistor
   - [ ] Physically simulated heating and cooling - Not setup.
   - [ ] Fan that is PWM controlled all according to temperature
      - This was not made possible yet, because I didn't have time to play around with registers.
   - [ ] Menu entry to set temperature.
      - Not quite there yet.
- [x] A menu on the LCD display controlled by the numbered keypad.
   - [ ] One menu to control a multicoloured LED and/or one or more other "white" LEDs
   - [ ] Potentially one menu to change PIN
   - [ ] If there is time, a menu to go into a game played on the MAX7219 Module
- [x] One or more libraries
 
Instead of actually focusing on getting the project done, I ended up reading about a lot and learning a fair bit of C instead.

It was exciting, but look where that lead me.
