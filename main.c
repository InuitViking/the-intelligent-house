/* 
 * File:   main.c
 * Author: Angutivik Casper Rúnur Tausen
 *
 * Created on December 7, 2020, 9:23 AM
 */

/*
 * External/Global variables
 */
extern volatile char currentPressedKey;
extern volatile float preferredTemp;
extern volatile int menuReturn;
extern volatile float currentTemp;
extern volatile char *currentTempStr;

/*
 * Macros
 */
#define F_CPU			16000000UL
#define FAN_ON			PORTB = 0b10000000
#define FAN_OFF			PORTB = 0x00
/*
 * System headers
 */
#include <stdio.h>
#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <math.h>
#include <string.h>

/*
 * Library headers
 */
#include "libs/functions/include/libtih.h"
#include "libs/lcdlibrary/lcd.h"

/*
 * Function headers
 */
#include "functions/init.h"
#include "functions/keypad.h"
#include "functions/login.h"
#include "functions/menu.h"
#include "functions/door.h"
#include "functions/temperature.h"


void Timer_80msDelay(){
	TCCR1B |= (1<<WGM12);		// Mode 4, CTC = WGM10 - WGM13 = 0100, Table 20-6 (page 176)
	TIMSK1 |= (1<<OCIE1A);		// Timer/Counter1, Output Compare A Match Interrupt Enable, 20.15.8 (page 184)
	OCR1A = 5000;				// 80ms delay
	TCCR1B |= (1<<CS12);		// Prescaler: 256, CS=100, Table 20-7 (page 177)
}

/*
 * Interrupts
 */

ISR(ADC_vect){
	unsigned int ADC_data = ADC;
	
	float temp = log(10000.0 * ( ( 1024.0/ADC_data ) -1 ) );												// Convert ADC_data to a value we can use.
	float tempK = 1 / ( 0.001129148 + ( 0.000234125 + ( 0.0000000876741 * temp * temp ) ) * temp );			// Calculate temperature to Kelvin.
	float tempC = tempK - 273.15;																			// Simply convert Kelvin to Celsius.

	char buffer[7];
	
	dtostrf(tempC,4,2,buffer);

	currentTempStr = buffer;
	currentTemp = tempC;

	if(currentTemp > preferredTemp){
		FAN_ON;
	}else{
		FAN_OFF;
	}
}

ISR(TIMER1_COMPA_vect){
	TIMSK1	&= ~(1<<OCIE1A);		// Disable Timer/Counter 1 Interrupt Mask Register - 17.11.33 TIMSK1
	TCCR1B	= 0;					// No clock source. See table 17-16 in chapter 17.11.8 Bit 2:0 - CSn2:0: Clock Select
	PCICR	|= (1<<PCIE2);		// 15.2.5 - Bit 2 - PCIE2: Pin Change Interrupt Enable 1 (PCINT23:16)
	PCMSK2	|= ((1<<PCINT23) | (1<<PCINT22) | (1<<PCINT21) | (1<<PCINT20));		// Pin Change Mask Register 2 (15.2.7 page 113) - Enable the higher nibble (set to 1)

	menu(currentPressedKey);
	
	if(menuReturn == 2){
		temp_management();
	}	
}

ISR(PCINT2_vect){
	PCICR	&= ~(1<<PCIE2);		// 15.2.5 - Bit 2 - PCIE2: Pin Change Interrupt Enable 1 (PCINT23:16) - Disable.
	PCMSK2	&= ~((1<<PCINT23) | (1<<PCINT22) | (1<<PCINT21) | (1<<PCINT20));	// Pin Change Mask Register 2 (15.2.7 page 113) - Disable the higher nibble (set to 0)

	int key;						// Create a variable which is to hold the number of the key pressed.
	char temporary = PINK;			// Set a temporary variable equal to input pins K
	temporary &= 0xF0;				// Set the higher nibbles in PINK

	
	if(temporary == 0x70){			// 01110000
		key = 0;
	}else if(temporary == 0xB0){	// 10110000
		key = 4;
	}else if(temporary == 0xD0){	// 11010000
		key = 8;
	}else if(temporary == 0xE0){	// 11100000
		key = 12;
	}else{
		key = 255;
	}

	if(key < 255){
		DDRK	= 0;				// Zero all bits
		PORTK	= temporary | 0x0F;	// Enable Row pins - Pull-up
		DDRK	= 0xF0;				// Use PortK. Upper nibble is rows, lower nibble is columns
		
		_delay_ms(100);
		
		temporary = PINK;
		temporary &= 0x0F;
		
		if(temporary == 0x07){			// 00000111
			key += 0;
		}else if(temporary == 0x0B){	// 00001011
			key += 1;
		}else if(temporary == 0x0D){	// 00001101
			key += 2;
		}else if(temporary == 0x0E){	// 00001110
			key += 3;
		}else{
			key += 0;
		}
		
		char keys[16] = {'1','2','3','A','4','5','6','B','7','8','9','C','*','0','#','D'};
		
		if( (int)key >= 0 && (int)key <= 15 ){

			currentPressedKey = keys[(int)key];
			
		}

		DDRK	= 0;
		PORTK	= 0xF0;
		DDRK	= 0x0F;
	}

	Timer_80msDelay();
}

int main(){
	// Set output to OC0A = PB7, see datasheet 16.9.4 and Arduino MEGA pin configuration: (pin 13), same as LED
	DDRB |= (1<<PB7);
	init();									// Initialize components and check permanent storage data integrity
	Timer_Millisecond(2000, F_CPU);
	
	sei();
	while(1);
}