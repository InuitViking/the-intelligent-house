/* 
 * File:   shadow.c
 * Author: Angutivik Casper Rúnur Tausen
 *
 * Created on December , 2020, 12:42 PM
 */

/*
 * Macros
 */
#define MEMEEWORD			0x1234
#define MEMEEPIN			"1234"

/*
 * System headers
 */
#include <avr/eeprom.h>

/*
 * These two were supposed to be used for storing the PIN in EEPROM,
 * but that was a failed experiment, as I don't have time to try to make it work.
 */
//uint16_t EEMEM eeword = MEMEEWORD;
//uint8_t EEMEM eepin[] = MEMEEPIN;