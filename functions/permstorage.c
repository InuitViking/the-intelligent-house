#define ARRAY_SIZE(arr)		(sizeof(arr) / sizeof(arr[0]))		// Get the size of the array

#define BLKSIZE				8

#include <avr/eeprom.h>
#include <avr/io.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

#include "../libs/lcdlibrary/lcd.h"

extern uint16_t EEMEM eeword;
extern uint8_t EEMEM eepin[];

/**
 * 
 * Reads the PIN from EEPROM
 * 
 * @return 
 */
//char * readPin(){
//	
//	lcd_clrscr();
//	lcd_puts("Reading EEPROM");
//	uint16_t sramword;
//	uint8_t readblock[BLKSIZE];
//	sramword = eeprom_read_word(&eeword);
//	eeprom_read_block ((void *)readblock, (const void *)eepin, BLKSIZE);
//	return &readblock;								// warning: return makes pointer from integer without a cast [-Wint-conversion]
//}

/**
 * 
 * Checks the integrity of the PIN
 * 
 * @return 
 */
//int checkPin(){
//
//	lcd_clrscr();
//	lcd_puts("Checking PIN");
//	lcd_gotoxy(0,1);
//	lcd_puts("integrity");
//
//
//	// Read PIN
//	char *pinPtr;
//	pinPtr = readPin();
//
//	// Check if the fourth (char is 1 byte) is a dot.
//	if((pinPtr + 4) == '.'){
//		lcd_clrscr();
//		lcd_puts("ERROR 0x0001");					// See documentation for what this means.
//		lcd_gotoxy(0,1);
//		lcd_puts("CONTACT TECH");
//		
//		lcd_putc((pinPtr + 4));
//		
//		return 0;									// Return 0 if pinPtr + 4 is not a dot (.) return 0 (false)
//	}
//
//	// Check if all of the PIN digits are integers.
//	for(int i = 0; i < 4; i++){
//		if(!isdigit(*(pinPtr + i))){
//			lcd_clrscr();
//			lcd_puts("ERROR 0x0002");				// See documentation for what this means.
//			lcd_gotoxy(0,1);
//			lcd_puts("CONTACT TECH");
//			return 0;								// If any of the characters are not a digit, return 0 (false)
//		}else{
//			lcd_putc(*(pinPtr + i));
//		}
//	}
//	
//	return 1;										// Return 1 (true) if all is well
//}