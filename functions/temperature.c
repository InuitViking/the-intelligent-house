/* 
 * File:   temperature.c
 * Author: ch
 *
 * Created on December 10, 2020, 4:49 PM
 */

/*
 * Custom headers
 */
#include "../libs/functions/include/libtih.h"
#include "../libs/lcdlibrary/lcd.h"

/*
 * System headers
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <ctype.h>
#include <string.h>
#include <util/delay.h>

extern volatile char currentPressedKey;
extern volatile float preferredTemp;
extern volatile int menuReturn;
extern volatile int tempCounter;
extern volatile char *currentTempStr;
extern volatile float currentTemp;

void temp_management(){
	
	lcd_clrscr();
	lcd_puts("Write temp:");
	lcd_gotoxy(0,1);
	char buffer[7];
	if(isdigit(currentPressedKey)){
				
		if(tempCounter == 0){
			preferredTemp = ((float)currentPressedKey - '0') * 10;
			dtostrf(preferredTemp,4,2,buffer);
			lcd_puts(buffer);
			tempCounter++;
		}else if(tempCounter == 1){
			preferredTemp += (float)currentPressedKey - '0';
			dtostrf(preferredTemp,4,2,buffer);
			lcd_puts(buffer);
			tempCounter = 0;
		}

		currentPressedKey = '.';
	}
}

void show_temp(){
	lcd_clrscr();
	lcd_puts("Current temp:");
	lcd_gotoxy(0,1);
	
	char buffer[7];
	
	dtostrf(currentTemp,4,2,buffer);
	
	lcd_puts(currentTempStr);
	_delay_ms(2000);
}