/* 
 * File:   login.c
 * Author: Angutivik Casper Rúnur Tausen
 *
 * Created on December 7, 2020, 12:57 PM
 */

/*
 * Macros
 */
#define ARRAY_SIZE(arr)		(sizeof(arr) / sizeof(arr[0]))	// Just a convenient way to get array size (number of elements in an array)
#define MEMEEPIN			"1234"							// See globals/shadow.c

/*
 * Custom headers
 * 
 * Has to come first, if we're using util/delay.h, because libtih has F_CPU defined
 */
#include "../libs/functions/include/libtih.h"
#include "../libs/lcdlibrary/lcd.h"

/*
 * System headers
 */
#include <ctype.h>			// For isdigit() function
//#include <avr/eeprom.h>	// For reading EEPROM
#include <string.h>			// For strcmp() function
#include <util/delay.h>		// For _delay_ms() function - read about it here: https://www.nongnu.org/avr-libc/user-manual/group__util__delay.html#gad22e7a36b80e2f917324dc43a425e9d3

/*
 * Function headers
 */
#include "../functions/keypad.h"
//#include "../functions/permstorage.h"			// See init.c for as to why this is outcommented


//extern uint16_t EEMEM eeword;					// See init.c for as to why this is outcommented
//extern uint8_t EEMEM eepin[];					// See init.c for as to why this is outcommented

void login(){
	
	while(1){									// We want an infinite loop, because we want to ask for PIN until user gets it right

		lcd_clrscr();							// Clear display and go home
		lcd_puts("Enter PIN:");					// Tell the user to enter a pin
		lcd_gotoxy(11,0);						// Go to column 11, row 0

		char pin[4];							// char array to hold PIN. Max amount is 4 (a string of maximum 4 characters.)
		int count = 0;							// Counter for how many times user has pressed a number key

		while(1){								// Infinite loop, because we have to keep listening for input
			KeyPadInit();
			ColumnScan();						// Scan columns on matrix keypad
			while(ReadRows() == '.'){}			// While the output is ".", do nothing

			if(isdigit(ReadRows())){			// If the output of ReadRows is a valid digit
				pin[count] = ReadRows();		// Put the output in pin
				lcd_putc('*');					// Print out an asterisk
				count++;						// Increment count
			}

			if(count == 4){						// If count reaches 4, break out of the infinite loop
				break;
			}
		}
		
		if(strcmp(pin,MEMEEPIN)){				// Compare written PIN with stored PIN
			lcd_clrscr();						// Clear display
			lcd_puts("Correct PIN!");			// Tell the user wrote the correct pin
			_delay_ms(1000);
			break;								// Break out of infinite loop
		}else{
			lcd_clrscr();						// Clear display
			lcd_puts("Try again!");				// Tell user to try again, because they wrote the wrong pin
			_delay_ms(1000);					// Wait 1 second before continuing loop
		}
	}
}

/**
 * This one currently doesn't work because I couldn't quite figure out EEPROM
 */
//void old_login(){
//	
//	while(1){									// Infinite loop, because we want to keep asking for the PIN until they get it right
//
//		char *pinPtr;
//		pinPtr = readPin();
//		
//		lcd_clrscr();							// Clear display and go home
//		lcd_puts("Enter PIN:");					// Tell the user to enter a pin
//		lcd_gotoxy(11,0);						// Go to column 11, row 0
//
//		lcd_putc((int)*(pinPtr + 0));
//		lcd_putc((pinPtr + 1));
//		lcd_putc((pinPtr + 2));
//		lcd_putc((pinPtr + 3));
//		
//		char pin[4];							// char array to hold PIN. Max amount is 4 (a string of maximum 4 characters.)
//		int count = 0;							// Counter for how many times user has pressed a number key
//
//		while(1){								// Infinite loop, because we have to keep listening for input
//			ColumnScan();						// Scan columns on matrix keypad
//			while(ReadRows() == '.'){}			// While the output is ".", do nothing
//
//			if(isdigit(ReadRows())){			// If the output of ReadRows is a valid digit
//				pin[count] = ReadRows();		// Put the output in pin
//				lcd_putc((pinPtr + count));					// Print out an asterisk
//				count++;						// Increment count
//			}
//
//			if(count == 4){						// If count reaches 4, break out of the infinite loop
//				break;
//			}
//		}
//
//		int savedPin[4];
//		
//		// For easier comparison between the two pins, we put the stored pin in a char array
//		for(int i = 0; i < 4; i++){
//			savedPin[i] = (int)*(readPin() + i);
//		}
//
//		if(strcmp(pin,savedPin) == 0){
//			lcd_clrscr();
//			lcd_puts("Correct PIN!");
//			Timer_Millisecond(2000, F_CPU);
//			break;
//		}else{
//			lcd_clrscr();
//			lcd_puts("Try again!");
//			Timer_Millisecond(2000, F_CPU);
//		}
//		
//	}
//
//}