/* 
 * File:   door.c
 * Author: ch
 *
 * Created on December 10, 2020, 3:52 PM
 */

#define pin1_ON (PORTH = 0x80)
#define pin2_ON (PORTH = 0x40)
#define pin3_ON (PORTH = 0x10)
#define pin4_ON (PORTH = 0x20)

#define pin1_OFF (PORTH = 0x00)
#define pin2_OFF (PORTH = 0x00)
#define pin3_OFF (PORTH = 0x00)
#define pin4_OFF (PORTH = 0x00)

/*
 * Custom headers
 */
#include "../libs/lcdlibrary/lcd.h"
#include "../libs/functions/include/libtih.h"

/*
 * System headers
 */
#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>

void setup() {
	// put your setup code here, to run ONce:
	DDRH = 0xF0;
}

void customDelay(uint8_t _speed){
	for(int i = 0; i < _speed; i++){
		_delay_ms(1);
	}
}

void forward (uint8_t _steps, uint8_t _speed){
	uint8_t n;
	for(n = 0; n < _steps ; n++){
		pin1_ON;
		pin4_OFF;
		customDelay(_speed);
		pin1_ON;
		pin2_ON;
		customDelay(_speed);
		pin1_OFF;
		pin2_ON;
		customDelay(_speed);
		pin2_ON;
		pin3_ON;
		customDelay(_speed);
		pin2_OFF;
		pin3_ON;
		customDelay(_speed);
		pin3_ON;
		pin4_ON;
		customDelay(_speed);
		pin3_OFF;
		pin4_ON;
		customDelay(_speed);
		pin4_ON;
		pin1_ON;
		customDelay(_speed);
	}
}

void reverse (uint8_t _steps,uint8_t _speed){
	uint8_t n;
	for (n = 0;n < _steps; n++){
		pin4_ON;
		pin1_OFF;
		customDelay(_speed);
		pin4_ON;
		pin3_ON;
		customDelay(_speed);
		pin4_OFF;
		pin3_ON;
		customDelay(_speed);
		pin3_ON;
		pin2_ON;
		customDelay(_speed);
		pin3_OFF;
		pin2_ON;
		customDelay(_speed);
		pin2_ON;
		pin1_ON;
		customDelay(_speed);
		pin2_OFF;
		pin1_ON;
		customDelay(_speed);
		pin1_ON;
		pin4_ON;
		customDelay(_speed);
	}
}

void loop(){
	forward(20,10);
	_delay_ms(1000);
	reverse(20,10);
	_delay_ms(1000);
}

void stepper_left(){
	setup();
	forward(20,10);
}

void stepper_right(){
	setup();
	reverse(20,10);
}

void unlock_door(){
	stepper_left();
}

void lock_door(){
	stepper_right();
}