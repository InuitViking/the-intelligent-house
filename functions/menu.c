/* 
 * File:   main.c
 * Author: Angutivik Casper Rúnur Tausen
 *
 * Created on December 9, 2020, 3:31 PM
 */

/*
 * External/Global variables
 */
extern volatile char currentPressedKey;
extern volatile int menuReturn;

/*
 * Custom headers
 * 
 * Has to come first, if we're using util/delay.h, because libtih has F_CPU defined
 */
#include "../libs/lcdlibrary/lcd.h"
#include "../libs/functions/include/libtih.h"

/*
 * System headers
 */
#include <avr/io.h>
#include <util/delay.h>		// For _delay_ms() function

/*
 * Function headers
 */
#include "../functions/keypad.h"
#include "../functions/login.h"
#include "../functions/door.h"
#include "../functions/temperature.h"

int menu(char key){

	if(key == '.'){
		lcd_clrscr();
		lcd_puts("Choose a menu");
		lcd_gotoxy(0,1);
		lcd_puts("with A B C D * #");
	}else if(key == 'A'){
		//login();
		lcd_clrscr();
		lcd_puts("Unlocking door.");
		unlock_door();
		_delay_ms(1500);
		currentPressedKey = '.';
	}else if(key == 'B'){
		//login();
		lcd_clrscr();
		lcd_puts("Locking door.");
		lock_door();
		_delay_ms(1500);
		currentPressedKey = '.';
	}else if(key == 'C'){
		//login();
		lcd_clrscr();
		lcd_puts("Set temperature.");
		currentPressedKey = '.';
		menuReturn = 2;
		_delay_ms(1500);
		return 2;
	}else if(key == 'D'){
		currentPressedKey = '.';
		show_temp();
	}
	
	return 1;
}	
	
	
//	ColumnScan();							// Scan columns on matrix keypad
//	while(ReadRows() == '.'){}				// While the output is ".", do nothing
//		
//	if(ReadRows() == 'A'){
//		login();
//		lcd_clrscr();
//		lcd_puts("Unlocking door.");
//	}else if(ReadRows() == 'B'){
//		login();
//		lcd_clrscr();
//		lcd_puts("Locking door.");
//	}else if(ReadRows() == 'C'){
//		login();
//		lcd_clrscr();
//		lcd_puts("Set temperature.");
//	}
