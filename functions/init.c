/* 
 * File:   init.c
 * Author: Angutivik Casper Rúnur Tausen
 *
 * Created on December , 2020, 12:42 PM
 */

/*
 * Custom headers
 * 
 * Has to come first, if we're using util/delay.h, because libtih has F_CPU defined
 */
#include "../libs/lcdlibrary/lcd.h"
#include "../libs/functions/include/libtih.h"

/*
 * System headers
 */
#include <avr/io.h>
//#include <avr/wdt.h>		// Watchdog and restarting of arduino board. Was to be used in case of errors
#include <util/delay.h>		// For _delay_ms() function

/*
 * Function headers
 */
//#include "../functions/permstorage.h"
#include "../functions/keypad.h"

/**
 * 
 * This function initializes components and checks PIN integrity.
 * 
 * @return 
 */
void init(){

	lcd_init(LCD_DISP_ON_CURSOR_BLINK);		// Initialize the LCD display
	lcd_clrscr();							// Clear screen
	
	// Initialize keypad
	lcd_puts("Initing keypad");				// Has to be a typo... Won't fit on display otherwise.
	_delay_ms(500);
	KeyPadInit();
	
	ADC_init();
	
	/*
	 * I failed to get a proper hold of EEPROM (for now).
	 * 
	 */
	// Check that a PIN valid in EEPROM
	//if(checkPin() == 0){
	//	wdt_enable(WDTO_8S);				// Enable a watchdog and do something in 8 watchdog seconds
	//	wdt_reset();						// Reset in the hopes that errors fix themselves
	//}
}