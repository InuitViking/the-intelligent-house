/*
 * File:   keypad.c
 * Author: Angutivik Casper Rúnur Tausen
 *
 * Created on December 7, 2020, 15:35 PM
*/

/*
 * Custom headers
 */
#include "../libs/lcdlibrary/lcd.h"
#include "../libs/functions/include/libtih.h"

/*
 * System headers
 */
#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

void KeyPadInit(){
	PORTK	= 0xF0;			// Enable Row pins - Pull-up
	DDRK	= 0x0F;			// Use PortK. Upper nibble is rows, lower nibble is columns
}
