#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/the-intelligent-house.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/the-intelligent-house.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=/home/ch/MPLABXProjects/the-intelligent-house.X/functions/keypad.c /home/ch/MPLABXProjects/the-intelligent-house.X/functions/login.c /home/ch/MPLABXProjects/the-intelligent-house.X/functions/init.c /home/ch/MPLABXProjects/the-intelligent-house.X/functions/permstorage.c /home/ch/MPLABXProjects/the-intelligent-house.X/functions/menu.c /home/ch/MPLABXProjects/the-intelligent-house.X/functions/door.c /home/ch/MPLABXProjects/the-intelligent-house.X/functions/temperature.c /home/ch/MPLABXProjects/the-intelligent-house.X/globals/shadow.c /home/ch/MPLABXProjects/the-intelligent-house.X/globals/variables.c /home/ch/MPLABXProjects/the-intelligent-house.X/libs/lcdlibrary/lcd.c /home/ch/MPLABXProjects/the-intelligent-house.X/main.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1761650682/keypad.o ${OBJECTDIR}/_ext/1761650682/login.o ${OBJECTDIR}/_ext/1761650682/init.o ${OBJECTDIR}/_ext/1761650682/permstorage.o ${OBJECTDIR}/_ext/1761650682/menu.o ${OBJECTDIR}/_ext/1761650682/door.o ${OBJECTDIR}/_ext/1761650682/temperature.o ${OBJECTDIR}/_ext/146202715/shadow.o ${OBJECTDIR}/_ext/146202715/variables.o ${OBJECTDIR}/_ext/1323992218/lcd.o ${OBJECTDIR}/_ext/1165167674/main.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1761650682/keypad.o.d ${OBJECTDIR}/_ext/1761650682/login.o.d ${OBJECTDIR}/_ext/1761650682/init.o.d ${OBJECTDIR}/_ext/1761650682/permstorage.o.d ${OBJECTDIR}/_ext/1761650682/menu.o.d ${OBJECTDIR}/_ext/1761650682/door.o.d ${OBJECTDIR}/_ext/1761650682/temperature.o.d ${OBJECTDIR}/_ext/146202715/shadow.o.d ${OBJECTDIR}/_ext/146202715/variables.o.d ${OBJECTDIR}/_ext/1323992218/lcd.o.d ${OBJECTDIR}/_ext/1165167674/main.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1761650682/keypad.o ${OBJECTDIR}/_ext/1761650682/login.o ${OBJECTDIR}/_ext/1761650682/init.o ${OBJECTDIR}/_ext/1761650682/permstorage.o ${OBJECTDIR}/_ext/1761650682/menu.o ${OBJECTDIR}/_ext/1761650682/door.o ${OBJECTDIR}/_ext/1761650682/temperature.o ${OBJECTDIR}/_ext/146202715/shadow.o ${OBJECTDIR}/_ext/146202715/variables.o ${OBJECTDIR}/_ext/1323992218/lcd.o ${OBJECTDIR}/_ext/1165167674/main.o

# Source Files
SOURCEFILES=/home/ch/MPLABXProjects/the-intelligent-house.X/functions/keypad.c /home/ch/MPLABXProjects/the-intelligent-house.X/functions/login.c /home/ch/MPLABXProjects/the-intelligent-house.X/functions/init.c /home/ch/MPLABXProjects/the-intelligent-house.X/functions/permstorage.c /home/ch/MPLABXProjects/the-intelligent-house.X/functions/menu.c /home/ch/MPLABXProjects/the-intelligent-house.X/functions/door.c /home/ch/MPLABXProjects/the-intelligent-house.X/functions/temperature.c /home/ch/MPLABXProjects/the-intelligent-house.X/globals/shadow.c /home/ch/MPLABXProjects/the-intelligent-house.X/globals/variables.c /home/ch/MPLABXProjects/the-intelligent-house.X/libs/lcdlibrary/lcd.c /home/ch/MPLABXProjects/the-intelligent-house.X/main.c

# Pack Options 
PACK_COMPILER_OPTIONS=-I "${DFP_DIR}/include"
PACK_COMMON_OPTIONS=-B "${DFP_DIR}/gcc/dev/atmega2560"



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/the-intelligent-house.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=ATmega2560
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1761650682/keypad.o: /home/ch/MPLABXProjects/the-intelligent-house.X/functions/keypad.c  .generated_files/842f205e22d0ca0a5ebfe100de304c0ca5fa26c6.flag .generated_files/ddfa517e0ca51880949bac5ba657ccaa60a423b5.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1761650682" 
	@${RM} ${OBJECTDIR}/_ext/1761650682/keypad.o.d 
	@${RM} ${OBJECTDIR}/_ext/1761650682/keypad.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega2560 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1761650682/keypad.o.d" -MT "${OBJECTDIR}/_ext/1761650682/keypad.o.d" -MT ${OBJECTDIR}/_ext/1761650682/keypad.o  -o ${OBJECTDIR}/_ext/1761650682/keypad.o /home/ch/MPLABXProjects/the-intelligent-house.X/functions/keypad.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1761650682/login.o: /home/ch/MPLABXProjects/the-intelligent-house.X/functions/login.c  .generated_files/e91e5e49dadf2ec813536749eb6f690a08d94522.flag .generated_files/ddfa517e0ca51880949bac5ba657ccaa60a423b5.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1761650682" 
	@${RM} ${OBJECTDIR}/_ext/1761650682/login.o.d 
	@${RM} ${OBJECTDIR}/_ext/1761650682/login.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega2560 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1761650682/login.o.d" -MT "${OBJECTDIR}/_ext/1761650682/login.o.d" -MT ${OBJECTDIR}/_ext/1761650682/login.o  -o ${OBJECTDIR}/_ext/1761650682/login.o /home/ch/MPLABXProjects/the-intelligent-house.X/functions/login.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1761650682/init.o: /home/ch/MPLABXProjects/the-intelligent-house.X/functions/init.c  .generated_files/8d5deb25dfd20aab669ae8117d1557921274d9d6.flag .generated_files/ddfa517e0ca51880949bac5ba657ccaa60a423b5.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1761650682" 
	@${RM} ${OBJECTDIR}/_ext/1761650682/init.o.d 
	@${RM} ${OBJECTDIR}/_ext/1761650682/init.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega2560 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1761650682/init.o.d" -MT "${OBJECTDIR}/_ext/1761650682/init.o.d" -MT ${OBJECTDIR}/_ext/1761650682/init.o  -o ${OBJECTDIR}/_ext/1761650682/init.o /home/ch/MPLABXProjects/the-intelligent-house.X/functions/init.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1761650682/permstorage.o: /home/ch/MPLABXProjects/the-intelligent-house.X/functions/permstorage.c  .generated_files/546df434a1ff4a591e4e41589a285c988fbe9008.flag .generated_files/ddfa517e0ca51880949bac5ba657ccaa60a423b5.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1761650682" 
	@${RM} ${OBJECTDIR}/_ext/1761650682/permstorage.o.d 
	@${RM} ${OBJECTDIR}/_ext/1761650682/permstorage.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega2560 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1761650682/permstorage.o.d" -MT "${OBJECTDIR}/_ext/1761650682/permstorage.o.d" -MT ${OBJECTDIR}/_ext/1761650682/permstorage.o  -o ${OBJECTDIR}/_ext/1761650682/permstorage.o /home/ch/MPLABXProjects/the-intelligent-house.X/functions/permstorage.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1761650682/menu.o: /home/ch/MPLABXProjects/the-intelligent-house.X/functions/menu.c  .generated_files/73495d4924d38a2c0250174033a0ad236c3ca9af.flag .generated_files/ddfa517e0ca51880949bac5ba657ccaa60a423b5.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1761650682" 
	@${RM} ${OBJECTDIR}/_ext/1761650682/menu.o.d 
	@${RM} ${OBJECTDIR}/_ext/1761650682/menu.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega2560 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1761650682/menu.o.d" -MT "${OBJECTDIR}/_ext/1761650682/menu.o.d" -MT ${OBJECTDIR}/_ext/1761650682/menu.o  -o ${OBJECTDIR}/_ext/1761650682/menu.o /home/ch/MPLABXProjects/the-intelligent-house.X/functions/menu.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1761650682/door.o: /home/ch/MPLABXProjects/the-intelligent-house.X/functions/door.c  .generated_files/b673ce4c1ee375beca0d925dc848d5d99bf2adf.flag .generated_files/ddfa517e0ca51880949bac5ba657ccaa60a423b5.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1761650682" 
	@${RM} ${OBJECTDIR}/_ext/1761650682/door.o.d 
	@${RM} ${OBJECTDIR}/_ext/1761650682/door.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega2560 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1761650682/door.o.d" -MT "${OBJECTDIR}/_ext/1761650682/door.o.d" -MT ${OBJECTDIR}/_ext/1761650682/door.o  -o ${OBJECTDIR}/_ext/1761650682/door.o /home/ch/MPLABXProjects/the-intelligent-house.X/functions/door.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1761650682/temperature.o: /home/ch/MPLABXProjects/the-intelligent-house.X/functions/temperature.c  .generated_files/c1a1f37ad7cebb3aec9453199a1b748f7f9e74cc.flag .generated_files/ddfa517e0ca51880949bac5ba657ccaa60a423b5.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1761650682" 
	@${RM} ${OBJECTDIR}/_ext/1761650682/temperature.o.d 
	@${RM} ${OBJECTDIR}/_ext/1761650682/temperature.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega2560 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1761650682/temperature.o.d" -MT "${OBJECTDIR}/_ext/1761650682/temperature.o.d" -MT ${OBJECTDIR}/_ext/1761650682/temperature.o  -o ${OBJECTDIR}/_ext/1761650682/temperature.o /home/ch/MPLABXProjects/the-intelligent-house.X/functions/temperature.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/146202715/shadow.o: /home/ch/MPLABXProjects/the-intelligent-house.X/globals/shadow.c  .generated_files/afe9ff774be30a1129979e1a106f9c5f0c6291c4.flag .generated_files/ddfa517e0ca51880949bac5ba657ccaa60a423b5.flag
	@${MKDIR} "${OBJECTDIR}/_ext/146202715" 
	@${RM} ${OBJECTDIR}/_ext/146202715/shadow.o.d 
	@${RM} ${OBJECTDIR}/_ext/146202715/shadow.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega2560 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/146202715/shadow.o.d" -MT "${OBJECTDIR}/_ext/146202715/shadow.o.d" -MT ${OBJECTDIR}/_ext/146202715/shadow.o  -o ${OBJECTDIR}/_ext/146202715/shadow.o /home/ch/MPLABXProjects/the-intelligent-house.X/globals/shadow.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/146202715/variables.o: /home/ch/MPLABXProjects/the-intelligent-house.X/globals/variables.c  .generated_files/44230e2f2bec0e90033f81c88a89eb62ab55ad83.flag .generated_files/ddfa517e0ca51880949bac5ba657ccaa60a423b5.flag
	@${MKDIR} "${OBJECTDIR}/_ext/146202715" 
	@${RM} ${OBJECTDIR}/_ext/146202715/variables.o.d 
	@${RM} ${OBJECTDIR}/_ext/146202715/variables.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega2560 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/146202715/variables.o.d" -MT "${OBJECTDIR}/_ext/146202715/variables.o.d" -MT ${OBJECTDIR}/_ext/146202715/variables.o  -o ${OBJECTDIR}/_ext/146202715/variables.o /home/ch/MPLABXProjects/the-intelligent-house.X/globals/variables.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1323992218/lcd.o: /home/ch/MPLABXProjects/the-intelligent-house.X/libs/lcdlibrary/lcd.c  .generated_files/a19231f600f2b4db53cca15330ad28af060655a8.flag .generated_files/ddfa517e0ca51880949bac5ba657ccaa60a423b5.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1323992218" 
	@${RM} ${OBJECTDIR}/_ext/1323992218/lcd.o.d 
	@${RM} ${OBJECTDIR}/_ext/1323992218/lcd.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega2560 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1323992218/lcd.o.d" -MT "${OBJECTDIR}/_ext/1323992218/lcd.o.d" -MT ${OBJECTDIR}/_ext/1323992218/lcd.o  -o ${OBJECTDIR}/_ext/1323992218/lcd.o /home/ch/MPLABXProjects/the-intelligent-house.X/libs/lcdlibrary/lcd.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1165167674/main.o: /home/ch/MPLABXProjects/the-intelligent-house.X/main.c  .generated_files/2031e08dfba8c9d9f64cf216f847113bbc2973a5.flag .generated_files/ddfa517e0ca51880949bac5ba657ccaa60a423b5.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1165167674" 
	@${RM} ${OBJECTDIR}/_ext/1165167674/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1165167674/main.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega2560 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS} -g -DDEBUG  -gdwarf-2  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1165167674/main.o.d" -MT "${OBJECTDIR}/_ext/1165167674/main.o.d" -MT ${OBJECTDIR}/_ext/1165167674/main.o  -o ${OBJECTDIR}/_ext/1165167674/main.o /home/ch/MPLABXProjects/the-intelligent-house.X/main.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
else
${OBJECTDIR}/_ext/1761650682/keypad.o: /home/ch/MPLABXProjects/the-intelligent-house.X/functions/keypad.c  .generated_files/b1ccfc0e4e982e0b8dbef63e0fd4c34b865f9883.flag .generated_files/ddfa517e0ca51880949bac5ba657ccaa60a423b5.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1761650682" 
	@${RM} ${OBJECTDIR}/_ext/1761650682/keypad.o.d 
	@${RM} ${OBJECTDIR}/_ext/1761650682/keypad.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega2560 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1761650682/keypad.o.d" -MT "${OBJECTDIR}/_ext/1761650682/keypad.o.d" -MT ${OBJECTDIR}/_ext/1761650682/keypad.o  -o ${OBJECTDIR}/_ext/1761650682/keypad.o /home/ch/MPLABXProjects/the-intelligent-house.X/functions/keypad.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1761650682/login.o: /home/ch/MPLABXProjects/the-intelligent-house.X/functions/login.c  .generated_files/f932e0201c7669bc02dc78575e645fd9ef7ccc4d.flag .generated_files/ddfa517e0ca51880949bac5ba657ccaa60a423b5.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1761650682" 
	@${RM} ${OBJECTDIR}/_ext/1761650682/login.o.d 
	@${RM} ${OBJECTDIR}/_ext/1761650682/login.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega2560 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1761650682/login.o.d" -MT "${OBJECTDIR}/_ext/1761650682/login.o.d" -MT ${OBJECTDIR}/_ext/1761650682/login.o  -o ${OBJECTDIR}/_ext/1761650682/login.o /home/ch/MPLABXProjects/the-intelligent-house.X/functions/login.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1761650682/init.o: /home/ch/MPLABXProjects/the-intelligent-house.X/functions/init.c  .generated_files/789f4989a519c55b612457c7a61af3ff7f70310d.flag .generated_files/ddfa517e0ca51880949bac5ba657ccaa60a423b5.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1761650682" 
	@${RM} ${OBJECTDIR}/_ext/1761650682/init.o.d 
	@${RM} ${OBJECTDIR}/_ext/1761650682/init.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega2560 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1761650682/init.o.d" -MT "${OBJECTDIR}/_ext/1761650682/init.o.d" -MT ${OBJECTDIR}/_ext/1761650682/init.o  -o ${OBJECTDIR}/_ext/1761650682/init.o /home/ch/MPLABXProjects/the-intelligent-house.X/functions/init.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1761650682/permstorage.o: /home/ch/MPLABXProjects/the-intelligent-house.X/functions/permstorage.c  .generated_files/4f9262e04d5ab126cb9f0860f45254adeed9dd28.flag .generated_files/ddfa517e0ca51880949bac5ba657ccaa60a423b5.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1761650682" 
	@${RM} ${OBJECTDIR}/_ext/1761650682/permstorage.o.d 
	@${RM} ${OBJECTDIR}/_ext/1761650682/permstorage.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega2560 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1761650682/permstorage.o.d" -MT "${OBJECTDIR}/_ext/1761650682/permstorage.o.d" -MT ${OBJECTDIR}/_ext/1761650682/permstorage.o  -o ${OBJECTDIR}/_ext/1761650682/permstorage.o /home/ch/MPLABXProjects/the-intelligent-house.X/functions/permstorage.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1761650682/menu.o: /home/ch/MPLABXProjects/the-intelligent-house.X/functions/menu.c  .generated_files/dcdaba4866602324ec55eed8cd199f31af207ab1.flag .generated_files/ddfa517e0ca51880949bac5ba657ccaa60a423b5.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1761650682" 
	@${RM} ${OBJECTDIR}/_ext/1761650682/menu.o.d 
	@${RM} ${OBJECTDIR}/_ext/1761650682/menu.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega2560 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1761650682/menu.o.d" -MT "${OBJECTDIR}/_ext/1761650682/menu.o.d" -MT ${OBJECTDIR}/_ext/1761650682/menu.o  -o ${OBJECTDIR}/_ext/1761650682/menu.o /home/ch/MPLABXProjects/the-intelligent-house.X/functions/menu.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1761650682/door.o: /home/ch/MPLABXProjects/the-intelligent-house.X/functions/door.c  .generated_files/37381aeb15d0219a320e77006f70a65d7337c6e9.flag .generated_files/ddfa517e0ca51880949bac5ba657ccaa60a423b5.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1761650682" 
	@${RM} ${OBJECTDIR}/_ext/1761650682/door.o.d 
	@${RM} ${OBJECTDIR}/_ext/1761650682/door.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega2560 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1761650682/door.o.d" -MT "${OBJECTDIR}/_ext/1761650682/door.o.d" -MT ${OBJECTDIR}/_ext/1761650682/door.o  -o ${OBJECTDIR}/_ext/1761650682/door.o /home/ch/MPLABXProjects/the-intelligent-house.X/functions/door.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1761650682/temperature.o: /home/ch/MPLABXProjects/the-intelligent-house.X/functions/temperature.c  .generated_files/5a913187c95e6abad5832917fa6c312f2b0ad990.flag .generated_files/ddfa517e0ca51880949bac5ba657ccaa60a423b5.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1761650682" 
	@${RM} ${OBJECTDIR}/_ext/1761650682/temperature.o.d 
	@${RM} ${OBJECTDIR}/_ext/1761650682/temperature.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega2560 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1761650682/temperature.o.d" -MT "${OBJECTDIR}/_ext/1761650682/temperature.o.d" -MT ${OBJECTDIR}/_ext/1761650682/temperature.o  -o ${OBJECTDIR}/_ext/1761650682/temperature.o /home/ch/MPLABXProjects/the-intelligent-house.X/functions/temperature.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/146202715/shadow.o: /home/ch/MPLABXProjects/the-intelligent-house.X/globals/shadow.c  .generated_files/2ec62d2a40a0b4aba682470a9b64b31b601b427e.flag .generated_files/ddfa517e0ca51880949bac5ba657ccaa60a423b5.flag
	@${MKDIR} "${OBJECTDIR}/_ext/146202715" 
	@${RM} ${OBJECTDIR}/_ext/146202715/shadow.o.d 
	@${RM} ${OBJECTDIR}/_ext/146202715/shadow.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega2560 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/146202715/shadow.o.d" -MT "${OBJECTDIR}/_ext/146202715/shadow.o.d" -MT ${OBJECTDIR}/_ext/146202715/shadow.o  -o ${OBJECTDIR}/_ext/146202715/shadow.o /home/ch/MPLABXProjects/the-intelligent-house.X/globals/shadow.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/146202715/variables.o: /home/ch/MPLABXProjects/the-intelligent-house.X/globals/variables.c  .generated_files/6acd093ed51ed40eb12630b865ce2901299765a.flag .generated_files/ddfa517e0ca51880949bac5ba657ccaa60a423b5.flag
	@${MKDIR} "${OBJECTDIR}/_ext/146202715" 
	@${RM} ${OBJECTDIR}/_ext/146202715/variables.o.d 
	@${RM} ${OBJECTDIR}/_ext/146202715/variables.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega2560 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/146202715/variables.o.d" -MT "${OBJECTDIR}/_ext/146202715/variables.o.d" -MT ${OBJECTDIR}/_ext/146202715/variables.o  -o ${OBJECTDIR}/_ext/146202715/variables.o /home/ch/MPLABXProjects/the-intelligent-house.X/globals/variables.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1323992218/lcd.o: /home/ch/MPLABXProjects/the-intelligent-house.X/libs/lcdlibrary/lcd.c  .generated_files/876adf790a4e0829a02eb84a3f3538159d655784.flag .generated_files/ddfa517e0ca51880949bac5ba657ccaa60a423b5.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1323992218" 
	@${RM} ${OBJECTDIR}/_ext/1323992218/lcd.o.d 
	@${RM} ${OBJECTDIR}/_ext/1323992218/lcd.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega2560 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1323992218/lcd.o.d" -MT "${OBJECTDIR}/_ext/1323992218/lcd.o.d" -MT ${OBJECTDIR}/_ext/1323992218/lcd.o  -o ${OBJECTDIR}/_ext/1323992218/lcd.o /home/ch/MPLABXProjects/the-intelligent-house.X/libs/lcdlibrary/lcd.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1165167674/main.o: /home/ch/MPLABXProjects/the-intelligent-house.X/main.c  .generated_files/892f8aa5335bda355943abcb2538a9ed983fa44e.flag .generated_files/ddfa517e0ca51880949bac5ba657ccaa60a423b5.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1165167674" 
	@${RM} ${OBJECTDIR}/_ext/1165167674/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1165167674/main.o 
	 ${MP_CC}  $(MP_EXTRA_CC_PRE) -mmcu=atmega2560 ${PACK_COMPILER_OPTIONS} ${PACK_COMMON_OPTIONS}  -x c -c -D__$(MP_PROCESSOR_OPTION)__  -funsigned-char -funsigned-bitfields -O1 -ffunction-sections -fdata-sections -fpack-struct -fshort-enums -Wall -MD -MP -MF "${OBJECTDIR}/_ext/1165167674/main.o.d" -MT "${OBJECTDIR}/_ext/1165167674/main.o.d" -MT ${OBJECTDIR}/_ext/1165167674/main.o  -o ${OBJECTDIR}/_ext/1165167674/main.o /home/ch/MPLABXProjects/the-intelligent-house.X/main.c  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/the-intelligent-house.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  /home/ch/MPLABXProjects/the-intelligent-house.X/libs/functions/lib/libtih.X.a  
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -mmcu=atmega2560 ${PACK_COMMON_OPTIONS}   -gdwarf-2 -D__$(MP_PROCESSOR_OPTION)__  -Wl,-Map="dist/${CND_CONF}/${IMAGE_TYPE}/the-intelligent-house.X.${IMAGE_TYPE}.map"    -o dist/${CND_CONF}/${IMAGE_TYPE}/the-intelligent-house.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    /home/ch/MPLABXProjects/the-intelligent-house.X/libs/functions/lib/libtih.X.a  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1 -Wl,--gc-sections -Wl,--start-group  -Wl,-lm -Wl,--end-group 
	
	
	
	
	
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/the-intelligent-house.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  /home/ch/MPLABXProjects/the-intelligent-house.X/libs/functions/lib/libtih.X.a 
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -mmcu=atmega2560 ${PACK_COMMON_OPTIONS}  -D__$(MP_PROCESSOR_OPTION)__  -Wl,-Map="dist/${CND_CONF}/${IMAGE_TYPE}/the-intelligent-house.X.${IMAGE_TYPE}.map"    -o dist/${CND_CONF}/${IMAGE_TYPE}/the-intelligent-house.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    /home/ch/MPLABXProjects/the-intelligent-house.X/libs/functions/lib/libtih.X.a  -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION) -Wl,--gc-sections -Wl,--start-group  -Wl,-lm -Wl,--end-group 
	${MP_CC_DIR}/avr-objcopy -O ihex "dist/${CND_CONF}/${IMAGE_TYPE}/the-intelligent-house.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}" "dist/${CND_CONF}/${IMAGE_TYPE}/the-intelligent-house.X.${IMAGE_TYPE}.hex"
	
	
	
	
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
